package com.example.apprecibonomina;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class ReciboNominaActivity extends AppCompatActivity {

    private ReciboNomina recibo;
    private TextView lblNumRecibo, lblNombre, lblSubtotal, lblImpuestoPor, lblTotal;
    private EditText txtHorasNormal, txtHorasExtras;
    private RadioButton rdbCajero, rdbDriveThru, rdbCocina;
    private Button btnCalcular, btnLimpiar, btnSalir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_recibo_nomina);


        recibo = new ReciboNomina();
        iniciarComponentes();


        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String horasTrabNormalS = txtHorasNormal.getText().toString();
                String horasTrabExtrasS = txtHorasExtras.getText().toString();

                if (horasTrabNormalS.isEmpty() || horasTrabExtrasS.isEmpty()) {
                    Toast.makeText(getApplicationContext(), "Todos los campos son obligatorios", Toast.LENGTH_SHORT).show();
                    return;
                }

                float horasTrabNormal;
                float horasTrabExtra;
                try {
                    horasTrabNormal = Float.parseFloat(horasTrabNormalS);
                    horasTrabExtra = Float.parseFloat(horasTrabExtrasS);
                } catch (NumberFormatException e) {
                    Toast.makeText(getApplicationContext(), "Horas trabajadas deben ser números válidos", Toast.LENGTH_SHORT).show();
                    return;
                }

                int puesto = 0;
                if (rdbCajero.isChecked()) puesto = 1;
                else if (rdbDriveThru.isChecked()) puesto = 2;
                else if (rdbCocina.isChecked()) puesto = 3;
                else {
                    Toast.makeText(getApplicationContext(), "Debe seleccionar un puesto", Toast.LENGTH_SHORT).show();
                    return;
                }


                recibo.setHorasTrabNormal(horasTrabNormal);
                recibo.setHorasTrabExtra(horasTrabExtra);
                recibo.setPuesto(puesto);

                float subtotal = recibo.calcularSubtotal();
                float impuesto = recibo.calcularImpuesto();
                float total = recibo.calcularTotal();

                lblSubtotal.setText(String.valueOf("Subtotal: $" + subtotal));
                lblImpuestoPor.setText(String.valueOf("Impuesto porcentual: " + impuesto));
                lblTotal.setText(String.valueOf("Total:"  + total));

            }


        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtHorasNormal.setText("");
                txtHorasExtras.setText("");
                rdbCajero.setChecked(true);
                lblImpuestoPor.setText("Impuesto Porcentual: ");
                lblSubtotal.setText("Subtotal: ");
                lblTotal.setText("Total: ");
            }
        });
        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }
    public void iniciarComponentes(){
        lblNumRecibo = (TextView) findViewById(R.id.lblNumRecibo);
        lblNombre = (TextView) findViewById(R.id.lblNombre);

        txtHorasNormal = (EditText) findViewById(R.id.txtHorasNormal);
        txtHorasExtras = (EditText) findViewById(R.id.txtHorasExtras);

        rdbCajero = (RadioButton) findViewById(R.id.rdbCajero);
        rdbDriveThru = (RadioButton) findViewById(R.id.rdbDriveThru);
        rdbCocina = (RadioButton) findViewById(R.id.rdbCocina);

        lblSubtotal = (TextView) findViewById(R.id.lblSubtotal);
        lblImpuestoPor = (TextView) findViewById(R.id.lblImpuestoPor);
        lblTotal = (TextView) findViewById(R.id.lblTotal);

        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnSalir = (Button) findViewById(R.id.btnSalir);




        lblNumRecibo.setText("Numero de Recibo: " + String.valueOf(recibo.numFolio()));
        Bundle datos = getIntent().getExtras();
        String nombre = datos.getString("cliente");
        lblNombre.setText("Nombre: " + nombre);
    }
}