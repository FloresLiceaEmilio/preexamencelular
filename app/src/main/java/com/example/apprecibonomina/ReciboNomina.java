package com.example.apprecibonomina;

import java.io.Serializable;
import java.util.Random;

public class ReciboNomina implements Serializable {

    private int numRecibo, puesto;
    private String nombre;
    private float horasTrabNormal, horasTrabExtra;
    private float impuestoPorc;

    public ReciboNomina() {

    }

    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getHorasTrabNormal() {
        return horasTrabNormal;
    }

    public void setHorasTrabNormal(float horasTrabNormal) {
        this.horasTrabNormal = horasTrabNormal;
    }

    public float getHorasTrabExtra() {
        return horasTrabExtra;
    }

    public void setHorasTrabExtra(float horasTrabExtra) {
        this.horasTrabExtra = horasTrabExtra;
    }

    public float getImpuestoPorc() {
        return impuestoPorc;
    }

    public void setImpuestoPorc(float impuestoPorc) {
        this.impuestoPorc = impuestoPorc;
    }

    public int numFolio(){
        Random r = new Random();
        return r.nextInt(1000);
    }

    public float calcularSubtotal(){
        float pagoInicial= 200;
        float pagoBase = 0;
        if (puesto == 1){
             pagoBase = pagoInicial*1.20f;
        }
        else if(puesto == 2){
             pagoBase = pagoInicial*1.50f;
        }
        else if(puesto == 3){
             pagoBase = pagoInicial*2f;
        }
        return (pagoBase * horasTrabNormal) + (horasTrabExtra * pagoBase * 2);
    }
    public float calcularImpuesto(){

        this.impuestoPorc = .16f;
        return calcularSubtotal()*this.impuestoPorc;
    }
    public float calcularTotal() {
        return calcularSubtotal() - calcularImpuesto();
    }
}
